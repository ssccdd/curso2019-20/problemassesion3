﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 3

Problemas propuestos para la Sesión 3 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion3#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion3#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion3#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion3#grupo-4)

## Grupo 1

El ejercicio consiste en la realización de unos pedidos de ordenadores por parte de una lista de proveedores. Cada uno de los proveedores dispone de una lista de montadores que le suministran las diferentes partes para la construcción de los ordenadores. Los proveedores indicarán a los montadores que les suministren las piezas para la realización de un ordenador. Los montadores no realizarán una pieza hasta que no se la solicite su proveedor. La finalización de los pedidos tiene una fecha tope de entrega. Alcanzada la fecha de entrega los proveedores suministrarán la lista del pedido realizada. La herramienta de Java para la sincronización en este ejercicio es [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html) y es la herramienta para sincronizar los montadores con sus proveedores. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Fabricante`: Clase que representa a un fabricante para un `TipoComponente` de un `Ordenador`.
- `Ordenador`: Representa el elemento que se debe construir con cada uno de los `TipoComponente`.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan. Para el desarrollo del ejercicio se debe decidir los elementos de sincronización que serán necesarios y que formarán parte de las variables de instancia de las clases:

- `Montador`: Tiene un identificador asociado y un componente que es capaz de fabricar. El trabajo del montador es:
	- Mientas no se solicite su interrupción
		- Esperar a que el proveedor le solicite la fabricación de su componente asociado.
		- Fabricar el componente asociado.
		- Indicar que se ha completado la fabricación del componente.
	- Controlar cualquier otra excepción que se pueda provocar en la ejecución del montador dando una solución apropiada.

- `Proveedor`: Debe tener un identificador para diferenciarlo y una variable que permita recoger las unidades que compondrán el pedido de ordenadores. El trabajo del proveedor es:
	- Crear los montadores que le suministrarán los diferentes componentes de un ordenador. 
		- Un montador por cada `TipoComponente`.
		- Asociar a cada montador un hilo.
		- Ejecutar los hilos asociados a los montadores.
	- Añadir un ordenador al pedido cada vez, y para ello:
		- Comunicará a los montadores que pueden empezar a preparar un nuevo componente para un nuevo ordenador.
		- Esperará a que todos los componentes del ordenador estén disponibles antes de completar el montaje del ordenador.
		- Simulará el tiempo de montaje del ordenador.
	- Debe comprobar que no se ha solicitado la cancelación del pedido de ordenadores.
	- Antes de finalizar deberá indicar a los montadores que han terminado con su trabajo y esperar a que finalicen antes de presentar el pedido que ha realizado. Tiene que indicar si el pedido está incompleto o ha conseguido entregar todo lo que se le había solicitado.

- `Hilo princial`: Realizará los siguientes pasos:
	- Crear la lista de hilos para 10 proveedores
	- Para cada proveedor:
		- Cada proveedor tiene que realizar un pedido de entre 1 a 10 ordenadores.
		- Asociar un objeto `Thread` a cada proveedor para su ejecución.
	- Ejecutar los hilos.
	- Se suspende por 30 segundos para que se completen los pedidos.
	- Pasado este tiempo solicita la interrupción de los pedidos que no se hayan completado en ese tiempo.
	- Espera a que todos los hilos finalicen su ejecución antes de finalizar.





## Grupo 2

En una granja de rendering (render farm) se generan **escenas** que luego se renderizan. Todo el proceso se realiza concurrentemente. Hay **generadores de escenas** que recopilan todo lo necesario para renderizar una escena (modelos 3D, texturas, materiales, etc.) y lo ponen a disposición de los **renderizadores** que construyen las imágenes con la especificación de la escena, mediante una lista de escenas común. (En esta sesión las tareas son simuladas con sleep).

Para la realización de este ejercicio no será necesario crear ninguna clase, pero se deberá terminar la implementación de que se indican más adelante. Además, se utilizará **ReentrantLock** para resolver los problemas de exclusión mutua que se presenten y **CyclicBarrier** para la sincronización.

Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes`: Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Escena`: Clase que representa las escenas que se generan / renderizan.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan. Para el desarrollo del ejercicio se debe decidir los elementos de sincronización que serán necesarios y que formarán parte de las variables de instancia de las clases:

-   `GeneradorEscenas`: Tiene un identificador asociado que permite identificarlo.
	-   Tiene acceso a la lista de escenas pendientes.
	-   Genera Escenas y la añade a la lista.
		-   La generación de una escena consume un tiempo aleatorio de entre 1 y 6 segundos.
		-   La tarea de generar escenas no finaliza hasta que se solicite su interrupción.

-   `RenderizadorEscena` : Tiene un identificador asociado que permite identificarlo.
	-   Tiene acceso a la lista de escenas pendientes.
	-   Selecciona la siguiente escena disponible y la renderiza. (La tiene que quitar de la lista, para que una misma escena no sea renderizada dos veces).
		-   La renderización de una escena consume un tiempo de entre 4 y 9 segundos.
		-   La tarea de renderizar escenas finaliza cuando se completan 5 escenas.
		-   O cuando se solicite su interrupción.

-   `Hilo Principal`: Realizará los siguientes pasos:
	-   Crea la lista de escenas pendientes.
	-   Crea e inicia la ejecución de 4 Tareas `GeneradorEscenas`.
	-   Crea e inicia la ejecución de 8 Tareas `RenderizadorEscenas`.
	-   Esperará hasta que 4 Tareas `RenderizadorEscenas` estén completas.
	-   Solicita la interrupción de todas las tareas restantes.
	-   Espera hasta la finalización de todas las tareas.


## Grupo 3

El ejercicio consistirá en simular una serie de ciclos de ejecución para unas unidades de procesamiento que tendrá asociados sus propios núcleos de ejecución independientes de otras unidades de procesamiento. Para cada ciclo de ejecución los núcleos deberán sincronizarse para comenzar a la vez y a la finalización deberán volverse a sincronizar con la unidad de procesamiento. El hilo principal será el encargado de crear las unidades de procesamiento y cada una de ellas creará sus núcleos de ejecución. La herramienta de Java para la sincronización en este ejercicio es [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html) y será la **única herramienta permitida** en el ejercicio. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Proceso`: Representa al proceso que simulará su ejecución en una unidad de procesamiento.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `NucleoEjecucion`: Tendrá asociado un identificador, un `Proceso` y los elementos de sincronización necesarios.
	- Esperará a que todos los nucleos de una unidad de procesamiento estén sincronizados antes de empezar la ejecución del proceso.
	- Se simulará la ejecución del proceso.
	- Se esperará a que todos los núcleos han concluido con la ejecución de sus procesos antes de finalizar.
	- Hay que programar la solicitud de interrupción.

- `UnidadProcesamiento`: Tendrá asociado un identificador, el número de ciclos que debe realizar y los elementos de sincronización necesarios.
	- Para cada ciclo de ejecución:
		- Creará tantos `NucleEjecucion` como los indicados en `NUCLEOS`
			- Para cada uno se crea un `Proceso` asociado a ese núcleo de ejecución.
			- Se le asociará a un hilo para su ejecución y se iniciará su ejecución.
		- Simulará un `TIEMPO_GESTION` y se indicará a los núcleos de ejecución que pueden comenzar la ejecución de su proceso.
		- Esperará a que todos los núcleos de ejecución hayan finalizado y volverá a simular un `TIEMPO_GESTION` antes del siguiente ciclo de ejecución.
	- Se debe programar la solicitud de interrupción. Tiene que solicitar la interrupción de sus núcleos de procesamiento.
	- A la finalización, se debe indicar si ha sido normal o cancelada, tendrá que presentar la lista de procesos que ha creado la unidad de procesamiento con su estado.

- `Hilo principal`: Realizará las siguientes tareas:
	- Creará tantas unidades procesamiento como los indicados por `TOTAL_PROCESADORES`
	- Para cada unidad de procesamiento:
		- Tendrá un número de ciclos aleatorio como mínimo de `NUM_CICLOS` y de hasta `VARIACION`
		- Asociaremos un hilo a cada proceso.
	- Se ejecutarán los hilos
	- Esperará por un tiempo establecido en `TIEMPO_ESPERA`
	- Se solicita la interrupción de los hilos y se espera a que todos finalicen antes de dar por terminado el hilo principal.

## Grupo 4

El ejercicio consiste en que un número de promotores deberán completar una serie de etapas. El comienzo de cada una de las etapas debe estar sincronizado, es decir, todos los promotores deben empezar a la vez. Para cada etapa y promotor hay un número de instaladores asignado cada uno de ellos a una casa en la que instalarán un número de sensores. Para cada promotor, los instaladores deberán sincronizarse en la instalación de los sensores, es decir, todos los instaladores comenzarán la instalación de un sensor a la vez. La herramienta de Java para la sincronización en este ejercicio es [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html) que utilizarán promotores e instaladores. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Sensor`: Simula un tipo de sensor y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Casa`: Simula donde se instalará un sensor. Dispone de una distribución de habitaciones según el `TipoCasa` que viene definido en `Constantes`.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Instalador`: Tiene un identificador, un número de sensores que instalar, la casa donde los instalará y los elementos de sincronización necesarios.
	- Para cada sensor que debe instalar:
		- Deberá sincronizarse con el resto de instaladores para que el comienzo de la instalación sea al mismo tiempo.
		- Creará el sensor de un tipo aleatorio.
		- Añadirá el sensor a la casa si es posible, simulando el tiempo de instalación.
	- Cuando finalice con su ronda de instalación deberá indicarlo dándose de baja del proceso de sincronización.
	- Debe programarse un procedimiento de interrupción.

- `Promotor`: Tiene un identificador, un número de etapas que cumplir y los elementos de sincronización necesarios.
	- Para cada una de las etapas que tiene que cumplir:
		- Antes de empezar una etapa deberá sincronizarse con el resto de promotores.
		- En la etapa tendrá asociado un número de  `INSTALADORES` con un rango aleatorio `VARIACION`.
		- Cada instalador tendrá asociada una casa de tipo aleatorio para la instalación de un `NUM_SENSORES` con un rango aleatorio `VARIACION`.
		- Cada instalador tiene asociado un hilo para su ejecución.
		- Antes de terminar la etapa el promotor debe esperar a la finalización de sus instaladores.
	- Cuando finalice el total de etapas asignado deberá darse de baja del elemento de sincronización con el resto de promotores.
	- Debe presentar la lista de las casas en las que sus instaladores han realizado trabajos.
	- Se tiene que programar un procedimiento de interrupción. Tiene que interrumpir a los instaladores asignados y esperar a su finalización antes de que pueda finalizar el promotor.

- `Hilo principal`: Realizará las siguientes tareas:
	- Creará un número `PROMOTORES` con una `VARIACION` aleatoria.
		- Cada promotor tendrá un `NUM_ETAPAS` con una `VARIACIÓN` aleatoria.
		- Se le asociará un hilo para su ejecución.
	- Se ejecutarán todos los hilos.
	- Se esperará un `TIEMPO_ESPERA`.
	- Al la finalización de la espera se solicitará la interrupción de lo promotores.
	- Antes de finalizar deben haber finalizado todos los promotores.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU5NzQzNjQ3LC0yMDY5Mjc5NzY4LC0xMj
c5Njg5NzA0LDE0MjkxMDM4MV19
-->