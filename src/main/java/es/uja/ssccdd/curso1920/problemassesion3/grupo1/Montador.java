/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo1;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Montador implements Runnable {
    private final String iD;
    private final Fabricante componente;
    private final CyclicBarrier inicioFabricacion;
    private final CyclicBarrier finFabricacion;

    public Montador(String iD, Fabricante componente, CyclicBarrier inicioFabricacion, 
                                                        CyclicBarrier finFabricacion) {
        this.iD = iD;
        this.componente = componente;
        this.inicioFabricacion = inicioFabricacion;
        this.finFabricacion = finFabricacion;
    }

    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza su jornada de trabajo");        
        
        // Está preparando componentes hasta que se solicite su interrupción
        // para finalizar su ejecución
        try {
            while ( true ) 
                prepararComponente();
        } catch (InterruptedException | BrokenBarrierException ex) {
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Finaliza su jornada de trabajo");
        } 
    }

    public String getiD() {
        return iD;
    }

    public Fabricante getComponente() {
        return componente;
    }

    private void prepararComponente() throws InterruptedException, BrokenBarrierException {
        // Antes de empear comprobamos si ha finalizado la fabricacion
        if( Thread.currentThread().isInterrupted() )
            new InterruptedException();
        
        // Esperamos a que el proveedor solicite la construcción de un ordenador
        inicioFabricacion.await();
        
        // Simula la fabricación de un componente
        TimeUnit.SECONDS.sleep(componente.tiempoFabricacion());
        
        // Indica que se ha completado la tarea de fabricación
        finFabricacion.await();
    }
}
