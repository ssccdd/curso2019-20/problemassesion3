/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo1;

import es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.TipoComponente;


/**
 *
 * @author pedroj
 */
public class Fabricante {
    private final String iD;
    private final TipoComponente componente;

    public Fabricante(String iD, TipoComponente componente) {
        this.iD = iD;
        this.componente = componente;
    }

    public String getiD() {
        return iD;
    }

    public TipoComponente getComponente() {
        return componente;
    }
    
    public int tiempoFabricacion() {
        return componente.tiempoFabricacion();
    }

    @Override
    public String toString() {
        return "Vandedor{" + "iD=" + iD + ", componente=" + componente + '}';
    }
}
