/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.MIN_DURACION_GENERACION;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.VARIACION_DURACION;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Tarea que simula la generacion de escenas
 * @author fconde
 */
public class GeneradorEscenas implements Runnable {

    private final String id;
    private final ArrayList<Escena> listaEscenas;
    private final ReentrantLock lock;

    public GeneradorEscenas(String id, ArrayList<Escena> listaEscenas, ReentrantLock lock) {
        this.id = id;
        this.listaEscenas = listaEscenas;
        this.lock = lock;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "GeneradorEscenas{" + "id=" + id + '}';
    }
    
    @Override
    public void run() {
        System.out.println("   Hilo(" + id + "): Inicia su ejecución");
        // - Esta tarea no finaliza hasta que la cancelan, por tanto hay que usar
        //   un ciclo while con una bandera booleana que cambie cuando se solicita
        //   la interrupcion del hilo
        boolean cancelado = false;
        int contador = 0;
        while (!cancelado) {
            // - Simular un tiempo de generacion de escenas aleatorio. Durante
            //   ese tiempo se puede interrumpir la tarea
            try {
                int duracion = MIN_DURACION_GENERACION + aleatorio.nextInt(VARIACION_DURACION);
                TimeUnit.SECONDS.sleep(duracion);
            } catch (InterruptedException ex) {
                System.out.println("   Hilo(" + id + "): Cancela su ejecución");
                cancelado = true;
            }
            
            // - Una vez generada la escena se añade a la lista
            
            // - Antes de acceder a la lista de escenas, hay que asegurarse de
            //   que nadie mas esta accediendo a ella (se hace con lock)
            lock.lock();
            try {
                Escena escena = new Escena(id + "-" + contador++);
                listaEscenas.add(escena);
            } finally {
                // - Esto es muy importante ya que si no se desbloquea, ningun
                //   otro hilo podria acceder a la lista de escenas
                lock.unlock();
            }
        }
    }
    
}
