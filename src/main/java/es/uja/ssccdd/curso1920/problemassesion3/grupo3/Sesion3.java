/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.aleatorio;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.TOTAL_PROCESADORES;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.NUM_CICLOS;

/**
 *
 * @author pedroj
 */
public class Sesion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        Thread[] listaHilos;
        CountDownLatch finProcesadores;
        
        
        // Inicialización de las variables para la prueba
        listaHilos = new Thread[TOTAL_PROCESADORES];
        finProcesadores = new CountDownLatch(TOTAL_PROCESADORES);
        
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Creación de las unidades de procesamiento con el elemento de 
        // sincronización con el hilo principal
        for( int i = 0; i < TOTAL_PROCESADORES; i++ ) {
            int numCiclos = aleatorio.nextInt(VARIACION) + NUM_CICLOS;
            UnidadProcesamiento procesador = new UnidadProcesamiento("Procesador("+i+")",
                                                    numCiclos,finProcesadores);
            listaHilos[i] = new Thread(procesador, procesador.getiD());
        }
        
        // Ejecución de los procesos en cada unidad de procesamiento
        for( Thread hilo : listaHilos ) 
            hilo.start();
        
        // Espera por el tiemo establecido para que se completen las ejecuciones
        // de los procesos
        System.out.println("HILO-Principal Espera a la finalización de los procesos");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de las unidades de procesamiento que han excedido el tiempo
        System.out.println("HILO-Principal Solicita la finalización de los pedidos");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        finProcesadores.await();
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
