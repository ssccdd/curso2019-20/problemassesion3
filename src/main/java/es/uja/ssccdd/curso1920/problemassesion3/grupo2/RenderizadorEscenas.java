/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.ESCENAS_ANTES_DE_TERMINAR;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.VARIACION_DURACION;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Tarea que simula la renderizacion de escenas.
 * @author fconde
 */
public class RenderizadorEscenas implements Runnable {

    private final String id;
    private final CyclicBarrier barrier;
    private final ArrayList<Escena> listaEscenas;
    private final ReentrantLock lock;

    public RenderizadorEscenas(String id, CyclicBarrier barrier, 
                               ArrayList<Escena> listaEscenas,
                               ReentrantLock lock) {
        this.id = id;
        this.barrier = barrier;
        this.listaEscenas = listaEscenas;
        this.lock = lock;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "RenderizadorEscenas{" + "id=" + id + '}';
    }
    
    @Override
    public void run() {
        System.out.println(" - Hilo(" + id + 
                           "): Inicia su ejecución");
        Escena escena = null;
        boolean tengoEscena;
        int j = 0;
        while (j < ESCENAS_ANTES_DE_TERMINAR) {
            // - Se toma una escena del array para renderizarla
            tengoEscena = false;
            // - Antes de acceder a la lista de escenas, hay que asegurarse de
            //   que nadie mas esta accediendo a ella (se hace con lock)
            lock.lock();
            try {
                // - Esto es necesario porque puede que se hayan ejecutado antes
                //   hilos renderizadores antes que hilos generadores y puede
                //   que cuando se va a recuperar una escena todavia no haya ninguna
                if (!listaEscenas.isEmpty()) {
                    escena = listaEscenas.remove(0);
                    j++;
                    tengoEscena = true;
                }
            } finally {
                // - Esto es muy importante ya que si no se desbloquea, ningun
                //   otro hilo podria acceder a la lista de escenas
                lock.unlock();
            }
            
            // - Si se ha podido recuperar una escena, se renderiza (se simula
            //   con sleep). Si no, se sigue iterando en el while
            if (tengoEscena) {
                try {
                    int duracion = Constantes.MIN_DURACION_RENDERING + 
                                   aleatorio.nextInt(VARIACION_DURACION);
                    TimeUnit.SECONDS.sleep(duracion);
                    System.out.println(" - Hilo(" + id + "): Renderizando " + escena +
                                       " " + j + " de " + ESCENAS_ANTES_DE_TERMINAR);
                } catch (InterruptedException ex) {
                    // - Si se solicita la interrupcion del hilo hay que aceptarla
                    //   e informar de ello
                    System.out.println(" > Hilo(" + id +  "): Cancela su ejecución");
                    return;
                }
            }
        }
        
        // - Cuando acaba el while es que ya se tienen ESCENAS_ANTES_DE_TERMINAR
        //   completas. Hay que notificarlo a la barrera. Al hacerlo, si todavia
        //   no se han completado el numero de renderizadores necesario, este se
        //   quedara a la espera de que terminen los demas
        try {
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException ex) {
            // - Aqui no hay que atender estas interrupciones ya que no se va
            //   a interrumpir a un hilo hasta que se pase esta barrera
        }
        
        // - Si se sale por aqui es porque este hilo completo todas sus escenas
        //   (no fue cancelado)
        System.out.println(" > Hilo(" + id + "): Termina su ejecución normalmente");
    }
}
