/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.EstadoEjecucion.CANCELADO;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.EstadoEjecucion.EN_EJECUCION;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.EstadoEjecucion.FINALIZADO;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class NucleoEjecucion implements Runnable {
    private final String iD;
    private final Proceso proceso;
    private final CountDownLatch inicioEjecucion;
    private final CountDownLatch finEjecucion;

    public NucleoEjecucion(String iD, Proceso proceso, CountDownLatch inicioEjecucion, 
                                                       CountDownLatch finEjecucion) {
        this.iD = iD;
        this.proceso = proceso;
        this.inicioEjecucion = inicioEjecucion;
        this.finEjecucion = finEjecucion;
    }

    @Override
    public void run() {
        // El núcleo ejecutará el proceso
        try {
            ejecucionProceso();
        } catch (InterruptedException ex) {
            cancelacionProceso();
        } finally {
            finEjecucion.countDown(); 
        }
    }
    
    /**
     * Simula la ejecución de un proceso por parte de un núcleo del procesador
     * @throws InterruptedException 
     */
    private void ejecucionProceso() throws InterruptedException {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Inicio ejecución " + proceso);
        
        // Se sincronzan todos los núcleos con la unidad de procesamiento
        inicioEjecucion.await();
        
        // Simulamos la ejecución del proceso
        proceso.setEstado(EN_EJECUCION);
        TimeUnit.SECONDS.sleep(proceso.tiempoEjecucion());
        
        // Finaliza la ejecución y sinconizamos los núcleos con la unidad
        // de procesamiento
        proceso.setEstado(FINALIZADO);
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Finaliza ejecución " + proceso);
    }
    
    /**
     * Se cancela la ejecución del proceso en el núcleo del procesador
     */
    private void cancelacionProceso() {
        proceso.setEstado(CANCELADO);
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Cancelada ejecución " + proceso);
    }

    public String getiD() {
        return iD;
    }
}
