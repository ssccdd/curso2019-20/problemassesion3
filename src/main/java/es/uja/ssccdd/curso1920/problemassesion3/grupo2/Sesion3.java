/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.NUM_GENERADORES;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.NUM_RENDERIZADORES;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo2.Constantes.TAREAS_ANTES_DE_CANCELAR;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Hilo principal de la aplicacion
 * @author fconde
 */
public class Sesion3 {

   /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("** Hilo(PRINCIPAL): Ha iniciado la ejecución");
        
        // - Lista de escenas compartida por todos los hilos. Por eso se declara
        //   e inicializa en el main
        ArrayList<Escena> lista = new ArrayList<Escena>();
        // - ReentrantLock compartido por todos los hilos para que si uno lo
        //   activa, los demas se lo encuentren activado. Por eso se declara en
        //   el main
        ReentrantLock lock = new ReentrantLock();

        // - Construir la lista de hilos que contendran los asignadores --------
        Thread[] hilos;    
        hilos = new Thread[NUM_GENERADORES+NUM_RENDERIZADORES];
        
        // - Este hilo se ejecutara cuando haya cuatro renderizadores que hayan
        //   completado todas sus tareas
        Finalizador finalizador = new Finalizador("FINALIZADOR", hilos);
        
        // - Creamos una barrera de forma que el programa espere hasta que haya
        //   TAREAS_ANTES_DE_CANCELAR rasterizadores que hayan terminado todas
        //   sus escenas
        CyclicBarrier barrier = new CyclicBarrier(TAREAS_ANTES_DE_CANCELAR, finalizador);
        
        // - Creamos los generadores de escenas, los asociamos a un hilo y lo
        //   guardamos en el array de hilos
        for (int i=0; i<NUM_GENERADORES; i++) {
            GeneradorEscenas gen = new GeneradorEscenas("GEN-" + i, lista, lock);
            hilos[i] = new Thread(gen, gen.getId());
        }
        
        // - Creamos los renderizadores de escenas, los asociamos a un hilo y lo
        //   guardamos en el array de hilos
        for (int i=NUM_GENERADORES; i<NUM_GENERADORES + NUM_RENDERIZADORES; i++) {
            RenderizadorEscenas rend = new RenderizadorEscenas("RENDER-" + i, 
                                                               barrier, lista, lock);
            hilos[i] = new Thread(rend, rend.getId());
        }

        // - Arrancando todas los hilos (generadores y renderizadores)
        for (Thread hilo: hilos) {
            hilo.start();
        }
        
        System.out.println("** Hilo(PRINCIPAL): Esperando a que terminen los demas hilos");
        
        // - Esperando a que todos los hilos terminen
        for (Thread hilo: hilos) {
            try {
                hilo.join();
            } catch (InterruptedException ex) {
                // - En este ejemplo no necesitamos atender esta interrupcion ya
                //   que no se va a interrumpir al hilo principal
            }
        }
        
        System.out.println("** Hilo(PRINCIPAL): Ha finalizado la ejecucion");
    }
}
