/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.EstadoEjecucion.LISTO;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.NUCLEOS;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.TIEMPO_GESTION;
import es.uja.ssccdd.curso1920.problemassesion3.grupo3.Constantes.TipoProceso;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class UnidadProcesamiento implements Runnable {
    private final String iD;
    private final int numCiclos;
    private final CountDownLatch finProcesadores;
    private final ArrayList<Proceso> listaProcesos;
    private final Thread[] listaHilos;
    
    // Sincronización para los núcleos del procesador
    private CountDownLatch inicioCiclo;
    private CountDownLatch finCiclo;

    public UnidadProcesamiento(String iD, int numCiclos, CountDownLatch finProcesadores) {
        this.iD = iD;
        this.numCiclos = numCiclos;
        this.finProcesadores = finProcesadores;
        this.listaProcesos = new ArrayList();
        this.listaHilos = new Thread[NUCLEOS];
    }
    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza la ejecución de sus " + numCiclos + " ciclos asignados");
        
        try {
            for(int i = 0; i < numCiclos; i++) {
                inicioCicloEjecucion(i);
                finCicloEjecucion(i);
            }
            finEjecucion();
        } catch (InterruptedException ex) {
            cancelacionEjecucion();
        } finally {
            finProcesadores.countDown();
        }
    }

    private void inicioCicloEjecucion(int ciclo) throws InterruptedException {
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Se inicia el ciclo " + (ciclo+1) + " del procesador");
        
        // Creamos los elementos de sincronización para cada ciclo
        inicioCiclo = new CountDownLatch(INICIO);
        finCiclo = new CountDownLatch(NUCLEOS);
        
        // Creamos los procesos para que sean ejecutados en los núcleos del
        // procesador y los añadimos a la lista de procesos que han sido asignados
        for(int i = 0; i < NUCLEOS; i++) {
            Proceso proceso = new Proceso(ciclo*numCiclos+i,TipoProceso.getProceso());
            proceso.setEstado(LISTO);
            listaProcesos.add(proceso);
            NucleoEjecucion nucleo = new NucleoEjecucion(iD+"("+i+")",proceso,
                                                         inicioCiclo,finCiclo);
            listaHilos[i] = new Thread(nucleo,nucleo.getiD());
            listaHilos[i].start();
        }
        
        // Simulamos tiempo de gestión interna antes de inicial la ejecución
        TimeUnit.SECONDS.sleep(TIEMPO_GESTION);
        
        // Iniciamos la ejecución
        inicioCiclo.countDown();
    }
    
    private void finCicloEjecucion(int ciclo) throws InterruptedException {
        // Esperamos a la finalización de la ejecución de los procesos
        finCiclo.await();
        
        // Simulamos tiempo de gestión interna antes de completar el ciclo de ejecución
        TimeUnit.SECONDS.sleep(TIEMPO_GESTION);
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Finaliza el ciclo " + (ciclo+1) + " del procesador");
    }
    
    private void finEjecucion() {
        System.out.println("Se finalizado la ejecución del HILO-" +
                    Thread.currentThread().getName() + 
                    "\n\tLista de procesos \n\t\t" + listaProcesos);
    }
    
    private void cancelacionEjecucion() {
        // Finalizamos la ejecución de los núcleos
        for(Thread hilo : listaHilos)
            hilo.interrupt();
        
        // Esperamos a que finalicen todos los núcleos
        try {
            finCiclo.await();
        } catch (InterruptedException ex) {
            // No hacemos nada estamos terminando
        }
        
        System.out.println("Se ha cancelado la ejecución del HILO-" +
                    Thread.currentThread().getName() + 
                    "\n\tLista de procesos \n\t\t" + listaProcesos);
        
    }
    
    public String getiD() {
        return iD;
    }
}
