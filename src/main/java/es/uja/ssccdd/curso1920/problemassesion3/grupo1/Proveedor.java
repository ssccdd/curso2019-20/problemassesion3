/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.COMPONENTES;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final int unidadesPedido;
    private final ArrayList<Ordenador> pedido;

    public Proveedor(String iD, int unidadesPedido) {
        this.iD = iD;
        this.unidadesPedido = unidadesPedido;
        this.pedido = new ArrayList();
    }

    

    @Override
    public void run() {
        // Montadores y su hilos
        Montador[] listaMontadores = new Montador[COMPONENTES.length];
        Thread[] hilos = new Thread[COMPONENTES.length];
        
        // Elementos de sincronización para el montaje de un ordenador del pedido
        CyclicBarrier inicioMontaje = new CyclicBarrier(COMPONENTES.length+1);
        CyclicBarrier finMontaje = new CyclicBarrier(COMPONENTES.length+1);
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza ha preparar su pedido de " + unidadesPedido +
                    " ordenadores");
        
        try {
            iniciarMontadores(listaMontadores, hilos, inicioMontaje, finMontaje);
            // Realización del pedido de ordenadores
            for(int i = 0; i < unidadesPedido; i++) {
                iniciarMontaje(inicioMontaje);
                finalizarMontaje(listaMontadores, finMontaje);
            }
            
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } catch (BrokenBarrierException ex) {
            System.err.println("Se ha presentado un problema en la sincronización de los"
                    + "trabajos del HILO-" + Thread.currentThread().getName());
        } finally {
            // Nos aseguramos que pase lo que pase se muestre el pedido realizado
            // y finalicen los montadores asociados al proveedor
            entregarPedido(hilos);
        }
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Se crean los montadores asociados a un proveedor y se ejecutan en sus 
     * hilos
     * @param inicioMontaje
     *      Sincronización para comenzar con la fabricación de su componente
     * @param finMontaje 
     *      Sincronización para indicar la finalización de la fabricación del 
     *          componente
     */
    private void iniciarMontadores(Montador[] listaMontadores, Thread[] hilos, 
                        CyclicBarrier inicioMontaje, CyclicBarrier finMontaje) {
        for(int i = 0; i < COMPONENTES.length; i++) {
            Fabricante componente = new Fabricante(iD+" "+COMPONENTES[i].name(),
                                                   COMPONENTES[i]);
            Montador montador = new Montador(iD+"-Montador("+i+")", componente, 
                                                        inicioMontaje, finMontaje);
            hilos[i] = new Thread(montador,montador.getiD());
            listaMontadores[i] = montador;
            hilos[i].start();
        }
    }
    
    /**
     * Sincroniza al proveedor con sus montadores para que comiencen la fabricación
     * de los componentes de un ordenador
     * @param inicio
     *      Elemento de sincronización para el inicio de la fabricación
     * @throws InterruptedException
     * @throws BrokenBarrierException 
     */
    private void iniciarMontaje(CyclicBarrier inicio) 
                            throws InterruptedException, BrokenBarrierException {
        // Sincronizamos en el mismo punto a los montadores con el proveedor
        inicio.await();
        
        // Restauramos la sincronización para el siguiente ordenador del pedido
        inicio.reset();
    }
    
    /**
     * Recoge el componente fabricado por el montador, simula el montaje del 
     * ordenador y lo añade a la lista de pedidos.
     * @throws InterruptedException 
     */
    private void finalizarMontaje(Montador[] listaMontadores, CyclicBarrier fin) 
                            throws InterruptedException, BrokenBarrierException {
        // Esperamos a tener todos los componentes de los montadores
        fin.await();
        
        // Restauramos la sincronización para el siguiente ordenador del pedido
        fin.reset();
        
        // 
        // Comprobamos si nos han solicitado la cancelación
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        // Montamos el ordenador y simulamos el tiempo de montaje
        Ordenador ordenador = new Ordenador();
        ordenador.setiD(iD+pedido.size());
        for( Montador montador : listaMontadores )
            ordenador.addComponente(montador.getComponente());
        pedido.add(ordenador);
        TimeUnit.SECONDS.sleep(ordenador.tiempoMontaje());
    }
    
    /**
     * Presenta el pedido de ordenadores o un mensaje indicando que se ha
     * cancelado su pedido
     */
    private void entregarPedido(Thread[] hilos) {
        for( Thread hilo : hilos )
            hilo.interrupt();
        
        try {
            for( Thread hilo : hilos )
                hilo.join();
        } catch (InterruptedException ex) {
            // No se trata porque se está finalizando con la ejecución
        }
        
        if( pedido.size() == unidadesPedido )
            System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " Pedido solicitado: \n\t" + pedido);
        else
            System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " No se ha completado el pedido: \n\t" + pedido);
    }
}
