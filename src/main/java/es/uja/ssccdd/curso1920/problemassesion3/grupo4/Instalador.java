/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo4;

import es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.TipoSensor;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Instalador implements Runnable {
    private final String iD;
    private final int numSensores;
    private final Casa casa;
    private final Phaser instalarSensores;

    public Instalador(String iD, int numSensores, Casa casa, Phaser instalarSensores) {
        this.iD = iD;
        this.numSensores = numSensores;
        this.casa = casa;
        this.instalarSensores = instalarSensores;
    }

    @Override
    public void run() {
        inicioTrabajo();
        
        try {
            for(int i = 0; i < numSensores; i++)
                instalarSensor(i);
        } catch (InterruptedException ex) {
            cancelacionTrabajo();
        } finally {
            finTrabajo();
        }
    }
    
    private void inicioTrabajo() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza la instalación de " + numSensores +
                    " sensores en la casa " + casa.getiD());
    }

    private void instalarSensor(int nSensor) throws InterruptedException {
        // Todos los instaladores instalan el un sensor de forma sincronizada
        instalarSensores.awaitAdvanceInterruptibly(instalarSensores.arrive());
        
        // Generamos un sensor aleatorio para instalarlo en la casa
        Sensor sensor = new Sensor(nSensor, TipoSensor.getSensor());
        if( casa.addSensor(sensor) )
            // Simulamos el tiempo de instalación
            TimeUnit.SECONDS.sleep(sensor.getTiempoInstalacion());
    }
    
    private void cancelacionTrabajo() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Se cancela la instalación de los sensores en la casa "
                    + casa.getiD());
    }
    
    private void finTrabajo() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Finaliza la instalación de los sensores en la casa "
                    + casa.getiD());
        
        // Se indica que ha terminado y se elimina de la sincronización de los
        // instaladores que aún no han terminado
        instalarSensores.arriveAndDeregister();
    }
    
    public String getiD() {
        return iD;
    }
}
