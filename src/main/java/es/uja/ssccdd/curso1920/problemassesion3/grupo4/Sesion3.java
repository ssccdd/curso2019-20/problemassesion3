/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.NUM_CASAS;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.PROMOTORES;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.aleatorio;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        Thread[] listaHilos;
        Phaser trabajoPromotores;
        int numPromotores;
        
        
        // Inicialización de las variables para la prueba
        numPromotores = aleatorio.nextInt(VARIACION) + PROMOTORES;
        listaHilos = new Thread[numPromotores];
        trabajoPromotores = new Phaser(numPromotores);
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        
        // Se crean los promotores con un número de casas asignado y el
        // elemento de sincronización de los trabajos
        for(int i = 0; i < numPromotores; i++) {
            // Casas asignadas
            int numEtapas = aleatorio.nextInt(VARIACION) + NUM_ETAPAS;
            Promotor promotor = new Promotor("Promotor("+i+")",numEtapas,trabajoPromotores);
            listaHilos[i] = new Thread(promotor,promotor.getiD());
        }
        
        // Empieza el trabajo de los promotores
        for( Thread hilo : listaHilos ) 
            hilo.start();
        
        // Espera por el tiemo establecido para que se completen los trabajos
        // de los promotores
        System.out.println("HILO-Principal Espera a la finalización de los promotores");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación del trabajo de los promotores 
        // que han excedido el tiempo
        System.out.println("HILO-Principal Solicita la finalización de los promotores");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de los hilos
        for( Thread hilo : listaHilos ) 
           hilo.join();
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
