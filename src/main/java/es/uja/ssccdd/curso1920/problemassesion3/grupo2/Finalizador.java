/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo2;

/**
 * Esta tarea (Runnable) es llamada por CyclicBarrier cuando TAREAS_ANTES_DE_CANCELAR
 * terminan su ejecucion. Su mision es pedir la interrupcion de todos los hilos
 * @author fconde
 */
public class Finalizador implements Runnable {

    private final String id;
    private final Thread[] hilos;

    public Finalizador(String id, Thread[] hilos) {
        this.id = id;
        this.hilos = hilos;
    }
    
    @Override
    public void run() {
        System.out.println("## Hilo(" + id + "): Inicia su ejecución");
        for (Thread hilo: hilos) {
            hilo.interrupt();
        }
    }
    
}
