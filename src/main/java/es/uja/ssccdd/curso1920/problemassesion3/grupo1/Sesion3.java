/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.TOTAL_PROVEEDORES;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.UNIDADES_MAXIMAS;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo1.Constantes.aleatorio;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        Thread[] listaHilos;
        int unidadesPedido;
        Proveedor proveedor;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        listaHilos = new Thread[TOTAL_PROVEEDORES];
        
        // Se preparan los pedidos para los proveedores y se inicia su ejecución
        for( int i = 0; i < TOTAL_PROVEEDORES; i++) {
            unidadesPedido = aleatorio.nextInt(UNIDADES_MAXIMAS) + 1;
            proveedor = new Proveedor("Proveedor("+i+")",unidadesPedido);
            listaHilos[i] = new Thread(proveedor, proveedor.getiD());
        }
        
        // Ejecutar los hilos asociados a los proveedores
        for( Thread hilo : listaHilos )
            hilo.start();
        
        // Espera por el tiemo establecido para que se completen los pedidos
        System.out.println("HILO-Principal Espera a la finalización de los pedidos");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de los pedidos que han excedido el tiempo
        System.out.println("HILO-Principal Solicita la finalización de los pedidos");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Esperamos a la entrega de los pedidos de los proveedores
        for( Thread hilo : listaHilos ) 
            hilo.join();
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
