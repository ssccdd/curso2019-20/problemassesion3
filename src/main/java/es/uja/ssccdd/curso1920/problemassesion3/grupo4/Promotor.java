/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion3.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.INSTALADORES;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.NUM_SENSORES;
import es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.TipoCasa;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion3.grupo4.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.Phaser;

/**
 *
 * @author pedroj
 */
public class Promotor implements Runnable {
    private final String iD;
    private final int numEtapas;
    private final Phaser trabajoPromotores;
    private final ArrayList<Casa> listaCasas;
    private Thread[] listaHilos;
  
    public Promotor(String iD, int numEtapas, Phaser trabajoPromotores) {
        this.iD = iD;
        this.numEtapas = numEtapas;
        this.trabajoPromotores = trabajoPromotores;
        this.listaCasas = new ArrayList();
        this.listaHilos = null;
    }

    @Override
    public void run() {
        inicioTrabajo();
        
        try {
            
            for(int i = 0; i < numEtapas; i++) 
                instalarEtapa(i);
            
            presentarTrabajo();
        } catch (InterruptedException ex) {
            cancelacionTrabajo();
        } finally {
            finTrabajo();
        }
    }

    private void inicioTrabajo() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza la ejecución de sus " + numEtapas + " etapas asignadas");
    }
    
    private void instalarEtapa(int etapa) throws InterruptedException {
        Phaser instalarSensores;
        int numInstaladores;
        int numSensores;
        
        // Todos los promotores se sincronizan antes de empezar la etapa
        trabajoPromotores.awaitAdvanceInterruptibly(trabajoPromotores.arrive());
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                                        " Empieza la " + (etapa+1) + "ª etapa");
        
        // Elementos para cada instalador
        numInstaladores = aleatorio.nextInt(VARIACION) + INSTALADORES;
        listaHilos = new Thread[numInstaladores];
        instalarSensores = new Phaser(numInstaladores); 
        
        // Se crean los instaladores y se inicia su ejecución para esta etapa
        for(int i = 0; i < numInstaladores; i++) {
            Casa casa = new Casa(etapa*numEtapas+i,TipoCasa.getCasa());
            listaCasas.add(casa); // Añadimos la casa al promotor
            numSensores = aleatorio.nextInt(VARIACION) + NUM_SENSORES;
            Instalador instalador = new Instalador(iD+"("+i+")",numSensores,casa,
                                                                instalarSensores);
            listaHilos[i] = new Thread(instalador,instalador.getiD());
            listaHilos[i].start();
        }
        
        // Esperamos a que finalicen los instaladores antes se pasar a otra etapa
        for( Thread hilo : listaHilos )
            hilo.join();
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                                        " Finaliza la " + (etapa+1) + "ª etapa");
    }
    
    private void presentarTrabajo() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Se ha completado el trabajo y las casas instaladas son:\n\t" +
                    listadoCasas(listaCasas));
    }
    
    private void cancelacionTrabajo() {
        // Cancelamos la ejecución de los instaladores
        for( Thread hilo : listaHilos )
            hilo.interrupt();
       
        // Esperamos a que finalicen los instaladores
        for( Thread hilo : listaHilos )
            try {
                hilo.join();
            } catch (InterruptedException ex) {
                // No hacemos nada al estar terminando
            }
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Se ha cancelado el trabajo y las casas instaladas son:\n\t" +
                    listadoCasas(listaCasas));
    }
    
    private void finTrabajo() {
        // Indica que ha finalizado y se elimina de la sincronización del resto
        // de promotores
        trabajoPromotores.arriveAndDeregister();
    }
    
    private String listadoCasas(ArrayList<Casa> lista) {
        String resultado = "Lista de casas:\n\t";
        
        for( Casa casa : lista )
            resultado = resultado + casa + "\n\t";
        
        return resultado;
    }
    
    public String getiD() {
        return iD;
    }
}
